%% carrera_lcc.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% 2024-06-15

:- module(carrera_lcc, [
              materia/5,
              corr_cursadas/2,
              corr_aprobadas/2
          ]).
/** <module> carrera_lcc: Materias y correlatividades del plan LCC 2013

Resumen de materias y cursadas de la carrera Licenciatura en Ciencias de la Computación (LCC),
de la Facultad de Informática de la Universidad Nacional del Comahue (Plan Ord. CS. 1112/2013).

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

%! materia(?Id:number, ?ShortName:atom, ?Name:atom, Year:number, ?Cuatri:number) is det.
%
% Datos de las materias.
% @todo Agregar carga horaria.

% Primer Año
materia(lcc-1, algebra, 'Elementos de Algebra', 1, 1).
materia(lcc-2, rpa, 'Resolucion de Problemas y Algoritmos', 1, 1).
materia(lcc-3, ic, 'Introduccion a la Computacion', 1, 1).
materia(lcc-4, msi, 'Modelos y Sistemas de Informacion', 1, 1).

materia(lcc-5, desarrollo, 'Desarrollo de Algoritmos', 1, 2).
materia(lcc-6, lineal, 'Elementos de Algebra Lineal', 1, 2).
materia(lcc-7, etc, 'Elementos de Teoria de la Computacion', 1, 2).
materia(lcc-8, modelado, 'Modelado de Datos', 1, 2).

% Segundo Año
materia(lcc-9, calculo, 'Calculo Diferencial e Integral', 2, 1).
materia(lcc-10, poo, 'Programacion Orientada a Objetos', 2, 1).
materia(lcc-11, estructuras, 'Estructuras de Datos', 2, 1).
materia(lcc-12, tc1, 'Teoria de la Computacion I', 2, 1).
materia(lcc-13, ingles1, 'Ingles Tecnico I', 2, 1).

materia(lcc-14, metodos, 'Metodos Computacionales para el Calculo', 2, 2).
materia(lcc-15, concurrente, 'Programacion Concurrente', 2, 2).
materia(lcc-16, tc2, 'Teoria de la Computacion II', 2, 2).
materia(lcc-17, arquitectura1, 'Arquitecturas y Organizacion de Computadoras I', 2, 2).
materia(lcc-18, requerimientos, 'Ingenieria de Requerimientos', 2, 2).
materia(lcc-19, ingles2, 'Ingles Tecnico II', 2, 2).

% Tercer Año
materia(lcc-20, prob, 'Probabilidad y Estadistica', 3, 1).
materia(lcc-21, plp, 'Principios de Lenguajes de Programacion', 3, 1).
materia(lcc-22, so1, 'Sistemas Operativos I', 3, 1).
materia(lcc-23, dbd, 'Diseno de Bases de Datos', 3, 1).
materia(lcc-24, arqui, 'Arquitecturas de Software', 3, 1).

materia(lcc-25, analisis, 'Analisis de Algoritmos', 3, 2).
materia(lcc-26, laboratorio, 'Laboratorio de Programacion', 3, 2).
materia(lcc-27, logica, 'Logica para Ciencias de la Computacion', 3, 2).
materia(lcc-28, redes1, 'Redes de Computadoras I', 3, 2).
materia(lcc-29, gestion, 'Gestion de Proyectos de Desarrollo Software', 3, 2).
materia(lcc-30, gbd, 'Gestion de Bases de Datos', 3, 2).

% Cuarto Año
materia(lcc-31, declarativos, 'Lenguajes Declarativos', 4, 1).
materia(lcc-32, complejidad, 'Complejidad Computacional', 4, 1).
materia(lcc-33, paralelos, 'Sistemas Paralelos', 4, 1).
materia(lcc-34, esp_soft, 'Especificacion de Software', 4, 1).

materia(lcc-35, diseno, 'Diseno de Algoritmos', 4, 2).
materia(lcc-36, ia, 'Inteligencia Artificial', 4, 2).
materia(lcc-37, calp, 'Conceptos Avanzados de Lenguajes de Programacion', 4, 2).
materia(lcc-38, esp_met, 'Especificacion con Metodos Formales', 4, 2).
materia(lcc-39, apys, 'Aspectos Profesionales y Sociales', 4, 2).

% Quinto Año
materia(lcc-40, si, 'Sistemas Inteligentes', 5, 1).
materia(lcc-41, agentes, 'Agentes Inteligentes para la Web', 5, 1).
materia(lcc-42, dis_comp, 'Diseno de Compiladores e Interpretes', 5, 1).
materia(lcc-43, lab_pd, 'Laboratorio de Programacion Distribuida', 5, 1).

materia(lcc-44, lab_comp, 'Laboratorio de Compiladores e Interpretes', 5, 2).
materia(lcc-45, lab_ia, 'Laboratorio de Inteligencia Artificial', 5, 2).
materia(lcc-46, mineria, 'Tecnicas para Mineria de Datos', 5, 2).

%! corr_cursadas(+IdMateria1:number, ?IdMateria2:number) is det.
%
% Para cursar la materia IdMateria1 se necesita la IdMateria2 cursada.

% Primer Año
corr_cursadas(lcc-5, lcc-1).
corr_cursadas(lcc-5, lcc-2).
corr_cursadas(lcc-6, lcc-1).
corr_cursadas(lcc-7, lcc-1).
corr_cursadas(lcc-8, lcc-4).
corr_cursadas(lcc-8, lcc-2).

% Segundo Año
corr_cursadas(lcc-9, lcc-1).
corr_cursadas(lcc-10, lcc-5).
corr_cursadas(lcc-11, lcc-5).
corr_cursadas(lcc-11, lcc-7).
corr_cursadas(lcc-12, lcc-5).
corr_cursadas(lcc-12, lcc-7).
corr_cursadas(lcc-13, lcc-4).
corr_cursadas(lcc-13, lcc-3).
corr_cursadas(lcc-14, lcc-5).
corr_cursadas(lcc-14, lcc-9).
corr_cursadas(lcc-15, lcc-3).
corr_cursadas(lcc-15, lcc-10).
corr_cursadas(lcc-16, lcc-12).
corr_cursadas(lcc-17, lcc-12).
corr_cursadas(lcc-18, lcc-10).
corr_cursadas(lcc-19, lcc-13).

% Tercer Año
corr_cursadas(lcc-20, lcc-14).
corr_cursadas(lcc-21, lcc-10).
corr_cursadas(lcc-21, lcc-12).
corr_cursadas(lcc-22, lcc-11).
corr_cursadas(lcc-22, lcc-15).
corr_cursadas(lcc-22, lcc-17).
corr_cursadas(lcc-23, lcc-11).
corr_cursadas(lcc-23, lcc-18).
corr_cursadas(lcc-24, lcc-11).
corr_cursadas(lcc-24, lcc-18).
corr_cursadas(lcc-24, lcc-19).
corr_cursadas(lcc-25, lcc-20).
corr_cursadas(lcc-25, lcc-16).
corr_cursadas(lcc-26, lcc-11).
corr_cursadas(lcc-26, lcc-15).
corr_cursadas(lcc-27, lcc-11).
corr_cursadas(lcc-27, lcc-16).
corr_cursadas(lcc-27, lcc-19).
corr_cursadas(lcc-28, lcc-22).
corr_cursadas(lcc-29, lcc-24).
corr_cursadas(lcc-30, lcc-15).
corr_cursadas(lcc-30, lcc-23).

% Cuarto Año
corr_cursadas(lcc-31, lcc-21).
corr_cursadas(lcc-31, lcc-27).
corr_cursadas(lcc-32, lcc-27).
corr_cursadas(lcc-32, lcc-25).
corr_cursadas(lcc-33, lcc-22).
corr_cursadas(lcc-34, lcc-29).
corr_cursadas(lcc-35, lcc-25).
corr_cursadas(lcc-36, lcc-21).
corr_cursadas(lcc-37, lcc-15).
corr_cursadas(lcc-37, lcc-22).
corr_cursadas(lcc-38, lcc-34).
corr_cursadas(lcc-39, lcc-24).
corr_cursadas(lcc-39, lcc-26).
corr_cursadas(lcc-39, lcc-28).

% Quinto Año
corr_cursadas(lcc-40, lcc-36).
corr_cursadas(lcc-41, lcc-36).
corr_cursadas(lcc-42, lcc-37).
corr_cursadas(lcc-43, lcc-33).
corr_cursadas(lcc-44, lcc-26).
corr_cursadas(lcc-44, lcc-42).
corr_cursadas(lcc-45, lcc-31).
corr_cursadas(lcc-45, lcc-35).
corr_cursadas(lcc-45, lcc-40).
corr_cursadas(lcc-45, lcc-41).
corr_cursadas(lcc-46, lcc-40).
corr_cursadas(lcc-49, lcc-35).
corr_cursadas(lcc-49, lcc-36).
corr_cursadas(lcc-49, lcc-39).

%! corr_aprobadas(+IdMateria1:number, ?IdMateria2:number) is det.
%
% Para cursar la materia IdMateria1 se necesita la IdMateria2 cursada y aprobada.

% Segundo Año
corr_aprobadas(lcc-11, lcc-2).
corr_aprobadas(lcc-14, lcc-1).
corr_aprobadas(lcc-16, lcc-7).
corr_aprobadas(lcc-17, lcc-3).
corr_aprobadas(lcc-18, lcc-1).
corr_aprobadas(lcc-18, lcc-8).

% Tercer Año
corr_aprobadas(lcc-23, lcc-6).
corr_aprobadas(lcc-24, lcc-10).
corr_aprobadas(lcc-25, lcc-11).
corr_aprobadas(lcc-26, lcc-10).
corr_aprobadas(lcc-27, lcc-12).
corr_aprobadas(lcc-28, lcc-17).
corr_aprobadas(lcc-29, lcc-18).
corr_aprobadas(lcc-30, lcc-18).

% Cuarto Año
corr_aprobadas(lcc-31, lcc-11).
corr_aprobadas(lcc-32, lcc-16).
corr_aprobadas(lcc-33, lcc-17).
corr_aprobadas(lcc-34, lcc-23).
corr_aprobadas(lcc-34, lcc-24).
corr_aprobadas(lcc-36, lcc-27).
corr_aprobadas(lcc-37, lcc-21).
corr_aprobadas(lcc-38, lcc-27).

% Quinto Año
corr_aprobadas(lcc-40, lcc-20).
corr_aprobadas(lcc-41, lcc-21).
corr_aprobadas(lcc-43, lcc-22).
corr_aprobadas(lcc-43, lcc-28).
corr_aprobadas(lcc-49, lcc-29).
corr_aprobadas(lcc-49, lcc-28).
