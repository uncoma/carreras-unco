#!/usr/bin/env swipl

%% main.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% 2024-06-15

:- module(main, [
              puedo_cursar/1,
              puedo_rendir/1,
              todas_puedo_cursar/1,
              todas_puedo_rendir/1,
              print_puedo_cursar/0,
              print_puedo_rendir/0
          ]).

/** <module> main: Módulo para iniciar el programa.

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- use_module(carrera_lcc).
:- use_module(mis_cursadas).
:- use_module(dot_exporter).

:- initialization(main, main).

% opt_type(o, output, atom).
% opt_type(f, format, atom).

% opt_help(output, 'The output file path').
% opt_help(format, 'Export format, can be dot, latex, html.').

%! puedo_cursar(?Materia: term) is det.
%
% ¿Cuál Materia puedo cursar?
%
% @param Materia [term] Identificador de la materia (p. ej.: `lcc-1`).
puedo_cursar(Materia) :-
    materia(Materia, _, _, _, _),
    \+ curse(Materia),
    forall(corr_cursadas(Materia, Corr), curse(Corr)),
    forall(corr_aprobadas(Materia, Corr2), aprobe(Corr2)).

%! puedo_rendir(?Materia: term) is det.
%
% ¿Cuál Materia puedo rendir?
%
% @param Materia [term] Identificador de la materia (p. ej.: `lcc-1`).
puedo_rendir(Materia) :-
    materia(Materia, _, _, _, _),
    \+ aprobe(Materia), curse(Materia),
    forall(corr_cursadas(Materia, Corr), curse(Corr)),
    forall(corr_aprobadas(Materia, Corr2), aprobe(Corr2)).

%! todas_puedo_cursar(?Lst: list) is det.
%
% Retornar todas las materias que puedo cursar.
todas_puedo_cursar(Lst) :-
    findall(Materia, puedo_cursar(Materia), Lst).

%! todas_puedo_rendir(?Lst: list) is det.
%
% Retornar todas las materias que puedo rendir.
todas_puedo_rendir(Lst) :-
    findall(Materia, puedo_rendir(Materia), Lst).

%! print_materia(+Materia) is det.
%
% Imprimir en pantalla los datos de la materia dada.
%
% @param Materia [term] Identificador de la materia (p. ej.: `lcc-1`).
print_materia(Materia) :-
    materia(Materia, _, Nombre, A, Cuat),
    format('~w (~w año/~w): ~w', [Materia, A, Cuat, Nombre]), nl.

%! print_puedo_cursar is det
%
% Imprimir en pantalla las materias que puedo cursar.
print_puedo_cursar :-
    todas_puedo_cursar(Lst),
    maplist(print_materia, Lst).

%! print_puedo_rendir is det
%
% Imprimir en pantalla las materias que puedo rendir.
print_puedo_rendir :-
    todas_puedo_rendir(Lst),
    maplist(print_materia, Lst).

%! output_filenames(+Name: atom, -NameDot: atom, -NamePNG: atom) is det.
%
% Retornar el nombre de los archivos dot y PNG.
output_filenames(Name, NameDot, NamePNG) :-
    atom_concat(Name, '.dot', NameDot),
    atom_concat(Name, '.png', NamePNG).

%! main(+Arguments: list)
%
% Iniciar la generación del archivo dot con la KB actual.
main(ArgV) :-
    argv_options(ArgV, [OutputName|_Rest], _Options),
    output_filenames(OutputName, OutputDot, OutputPNG),

    format('Generando Dot: KB -> ~w', [OutputDot]), nl,

    to_dot_file(OutputDot),

    format('Generando PNG: ~w -> ~w', [OutputDot, OutputPNG]), nl,
    
    format(atom(Command), 'dot -Tpng -o "~w" "~w"', [OutputPNG, OutputDot]),
    shell(Command).
