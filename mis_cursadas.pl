%% mis_cursadas.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% 2024-06-15

:- module(mis_cursadas, [
              curse/1,
              aprobe/1
          ]).
/** <module> mis_cursadas: Mis cursadas.

Aquí se escriben mis materias cursadas y aprobadas.

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- multifile curse/1.

%! curse(?IdMateria:number) is det.
%
% La materia IdMateria ya la curse.
%
% Se puede crear sus propios curse/1 para definir sus propias
% materias cursadas y aprobadas en un archivo diferente.
curse(N) :-
    % caso: ¡si ya la aprobé es porque la cursé antes!
    aprobe(N), !.
%% Ejemplos para probar:
% curse(lcc-2).
% curse(lcc-5).

%! aprobe(?IdMateria:number) is det.
%
% La materia IdMateria ya la cursé y aprobé.
:- multifile aprobe/1.
%% Ejemplos para probar:
% aprobe(lcc-1).
