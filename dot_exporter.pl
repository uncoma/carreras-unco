%% carrera_lcc.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% 2024-06-15

:- module(dot_exporter, [
              to_dot/1,
              to_dot_file/1
          ]).
/** <module> dot_exporter: Exportar materias a dot.

La idea final es poder armar un grafo de dependencia de la carrera con mis cursadas marcadas.

# Mi KB de cursadas y aprobadas
Se puede crear un archivo aparte con mis propias cursadas y aprobadas. Este archivo debe
contener hechon con curse/1 y aprobe/1. Por ejemplo:

```
curse(2).
curse(5).

aprobe(1).
aprobe(3).
aprobe(7).
```

No es necesario declarar una materia como cursada si ya está declarda como aprobada. En el
ejemplo `aprobe(1)` está en la KB, pero no `curse(1)` porque sería redundante.

@todo Separar el conocimiento de materias, cursadas y aprobadas de la generación del grafo.
@todo Dar soporte para generar LaTeX, HTML u otro formato a partir de toda la info en la KB.
@todo Ordenar nodos de materias vertical/horizontalmente por años. Para eso, se necesitarán crear nodos blancos: `"  " -> "   " -> calculo;` (tercer año).

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- use_module(carrera_lcc).
:- use_module(mis_cursadas).

%! dot_header(?Str:string) is det.
%
% El header utilizado en archivos dot.
dot_header("digraph { year1 [style=invis]; year2 [style=invis]; year3 [style=invis]; year4 [style=invis]; year5 [style=invis];").

%! dot_footer(?Str:string) is det.
%
% El footer utilizado en archivos dot.
dot_footer("}").

%! corr_to_dot(?Mat1:number, ?Mat2:number, -Str:string) is semidet.
%
% Representar en sintaxis dot un arco entre Mat2 y Mat1, si estas son
% materias correlativas.
%
% @todo Representar con otro color las corr_aprobadas/2.
corr_to_dot(Mat1, Mat2, Str) :-
    corr_cursadas(Mat1, Mat2),
    materia(Mat1, Short1, _, _, _),
    materia(Mat2, Short2, _, _, _),
    format(string(Str), "~w -> ~w;\n", [Short2, Short1]).

aprob_to_dot(Mat1, Mat2, Str) :-
    corr_aprobadas(Mat1, Mat2),
    materia(Mat1, Short1, _, _, _),
    materia(Mat2, Short2, _, _, _),
    format(string(Str), "~w -> ~w [style=bold];\n", [Short2, Short1]).    

%! materia_to_color(+IdMat:number, -Color:atom) is det.
%
% Retornar el color de una materia dependiendo si es
% cursada, aprobada o ninguna de las dos.
materia_to_color(Mat, green) :-
    aprobe(Mat), !.
materia_to_color(Mat, blue) :-
    curse(Mat), !.
materia_to_color(_Mat, black).

%! materia_to_dot(+IdMat:number, -Str:string) is det.
%
% Representar una materia como un nodo en sintaxis dot.
materia_to_dot(Mat1, Str) :-
    materia(Mat1, Short1, _, _, _),
    materia_to_color(Mat1, Color),
    format(string(Str), "~w [color=~w];\n", [Short1, Color]).

materia_year_to_dot(Mat, Str) :-
    materia(Mat, Short, _, Year, _),
    format(string(Str), "year~w -> ~w [style=invis];\n", [Year, Short]).

%! string_concatenate(+LstStrings:list, -Str:string)
%
% Concatenar varios strings de una lista y retornar el resultado en uno solo.
string_concatenate([], "") :- !.
string_concatenate([S1|Rest], StrRest) :-
    string_concatenate(Rest, StrRest1),
    string_concat(S1, StrRest1, StrRest).

%! to_dot(-Str:string)
%
% Generar un string que represente en sintaxis dot las materias correlativas, las cursadas y aprobadas.
to_dot(Str) :-
    dot_header(Header),

    findall(Str, materia_to_dot(_Mat, Str), LstNodosStr),
    string_concatenate(LstNodosStr, NodosStr),

    findall(Str, materia_year_to_dot(_Mat0, Str), LstStr0),
    string_concatenate(LstStr0, YearStr),
    
    findall(Str, corr_to_dot(_Mat1, _Mat2, Str), LstStr1),
    string_concatenate(LstStr1, CorrStr),

    findall(Str, aprob_to_dot(_Mat1b, _Mat2b, Str), LstStr2),
    string_concatenate(LstStr2, AprobStr),
    
    dot_footer(Footer),
    format(string(Str), "~w\n~w\n\n~w\n~w\n~w\n~w\n",
           [Header, NodosStr, YearStr, CorrStr, AprobStr, Footer]).

%! to_dot_file(+Path:atom) is det.
%
% Escribir el grafo de dependencia en un archivo dot.
%
% Crear el archivo Path y agregarle el string necesarios para crear con dot (Graphviz)
% un grafo de dependencias.
to_dot_file(Path) :-
    to_dot(Str),

    tell(Path),
    write(Str),
    told.
