%% carrera_lsi.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% 2024-06-15

:- module(carrera_lsi, [
              materia/5,
              corr_cursadas/2,
              corr_aprobadas/2
          ]).
/** <module> carrera_lsi: Materias y correlatividades del plan LCC 2013

Resumen de materias y cursadas de la carrera Licenciatura en Ciencias de la Computación (LCC),
de la Facultad de Informática de la Universidad Nacional del Comahue (Plan Ord. CS. 1112/2013).

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

%! materia(?Id:number, ?ShortName:atom, ?Name:atom, Year:number, ?Cuatri:number) is det.
%
% Datos de las materias.
% @todo Agregar carga horaria.

% Primer Año
materia(lsi-1, algebra, 'Elementos de Algebra', 1, 1).
materia(lsi-2, rpa, 'Resolucion de Problemas y Algoritmos', 1, 1).
materia(lsi-3, ic, 'Introduccion a la Computacion', 1, 1).
materia(lsi-4, msi, 'Modelos y Sistemas de Informacion', 1, 1).

materia(lsi-5, desarrollo, 'Desarrollo de Algoritmos', 1, 2).
materia(lsi-6, lineal, 'Elementos de Algebra Lineal', 1, 2).
materia(lsi-7, etc, 'Elementos de Teoria de la Computacion', 1, 2).
materia(lsi-8, modelado, 'Modelado de Datos', 1, 2).

% Segundo Año
materia(lsi-9, calculo, 'Calculo Diferencial e Integral', 2, 1).
materia(lsi-10, poo, 'Programacion Orientada a Objetos', 2, 1).
materia(lsi-11, estructuras, 'Estructuras de Datos', 2, 1).
materia(lsi-12, tc1, 'Teoria de la Computacion I', 2, 1).
materia(lsi-13, ingles1, 'Ingles Tecnico I', 2, 1).

materia(lsi-14, metodos, 'Metodos Computacionales para el Calculo', 2, 2).
materia(lsi-15, concurrente, 'Programacion Concurrente', 2, 2).
materia(lsi-16, tc2, 'Teoria de la Computacion II', 2, 2).
materia(lsi-17, arquitectura1, 'Arquitecturas y Organizacion de Computadoras I', 2, 2).
materia(lsi-18, requerimientos, 'Ingenieria de Requerimientos', 2, 2).
materia(lsi-19, ingles2, 'Ingles Tecnico II', 2, 2).

% Tercer Año
materia(lsi-20, prob, 'Probabilidad y Estadistica', 3, 1).
materia(lsi-21, plp, 'Principios de Lenguajes de Programacion', 3, 1).
materia(lsi-22, so1, 'Sistemas Operativos I', 3, 1).
materia(lsi-23, dbd, 'Diseno de Bases de Datos', 3, 1).
materia(lsi-24, arqui, 'Arquitecturas de Software', 3, 1).

materia(lsi-25, analisis, 'Analisis de Algoritmos', 3, 2).
materia(lsi-26, laboratorio, 'Laboratorio de Programacion', 3, 2).
materia(lsi-27, logica, 'Logica para Ciencias de la Computacion', 3, 2).
materia(lsi-28, redes1, 'Redes de Computadoras I', 3, 2).
materia(lsi-29, gestion, 'Gestion de Proyectos de Desarrollo Software', 3, 2).
materia(lsi-30, gbd, 'Gestion de Bases de Datos', 3, 2).

% Cuarto Año
materia(lsi-31, pl_ctrl_proy, 'Planificación y Control de Proyectos', 4, 1).
materia(lsi-32, sis_inf, 'Sistemas de Información', 4, 1).
materia(lsi-33, mod_Nego, 'Modelado de Procesos de Negocios', 4, 1).
materia(lsi-34, esp_req, 'Especificacion de Requerimientos', 4, 1).
materia(lsi-35, sis_paralelos, 'Sistemas Paralelos', 4, 1).

materia(lsi-36, ia, 'Inteligencia Artificial', 4, 2).
materia(lsi-37, esp_dis, 'Especificación de Diseño de Software', 4, 2).
materia(lsi-38, min_datos, 'Deposito y Mineria de Datos', 4, 2).
materia(lsi-39, apys, 'Sistemas de Información II', 4, 2).
materia(lsi-40, lab_base_dat, 'Laboratorio de Base de Datos', 4, 2).

% Quinto Año

materia(lsi-41, sis_int, 'Sistemas Inteligentes', 5, 1).
materia(lsi-42, aud_sis_inf, 'Auditoria de los Sistemas de Información', 5, 1).
materia(lsi-43, ver_val_soft, 'Verificación y Validación de Software', 5, 1).
materia(lsi-44, lab_prog_dist, 'Laboratorio de Programacion Distribuida', 5, 1).

materia(lsi-45, sis_inf_web, 'Sistemas de Informacion para la Web', 5, 2).
materia(lsi-46, asp_prof, 'Aspectos Profesionales y Sociales', 5, 2).
materia(lsi-47, reing_soft, 'Reingenieria de Software y Procesos', 5, 2).
materia(lsi-48, const_val_soft, 'Construccion y Validación de Software', 5, 2).
materia(lsi-51, tesis, 'Trabajo de Tesis', 5, 2).

%! corr_cursadas(+IdMateria1:number, ?IdMateria2:number) is det.
%
% Para cursar la materia IdMateria1 se necesita la IdMateria2 cursada.

% Primer Año
corr_cursadas(lsi-5, lsi-1).
corr_cursadas(lsi-5, lsi-2).
corr_cursadas(lsi-6, lsi-1).
corr_cursadas(lsi-7, lsi-1).
corr_cursadas(lsi-8, lsi-4).
corr_cursadas(lsi-8, lsi-2).

% Segundo Año
corr_cursadas(lsi-9, lsi-1).
corr_cursadas(lsi-10, lsi-5).
corr_cursadas(lsi-11, lsi-5).
corr_cursadas(lsi-11, lsi-7).
corr_cursadas(lsi-12, lsi-5).
corr_cursadas(lsi-12, lsi-7).
corr_cursadas(lsi-13, lsi-4).
corr_cursadas(lsi-13, lsi-3).
corr_cursadas(lsi-14, lsi-5).
corr_cursadas(lsi-14, lsi-9).
corr_cursadas(lsi-15, lsi-3).
corr_cursadas(lsi-15, lsi-10).
corr_cursadas(lsi-16, lsi-12).
corr_cursadas(lsi-17, lsi-12).
corr_cursadas(lsi-18, lsi-10).
corr_cursadas(lsi-19, lsi-13).

% Tercer Año
corr_cursadas(lsi-20, lsi-14).
corr_cursadas(lsi-21, lsi-10).
corr_cursadas(lsi-21, lsi-12).
corr_cursadas(lsi-22, lsi-11).
corr_cursadas(lsi-22, lsi-15).
corr_cursadas(lsi-22, lsi-17).
corr_cursadas(lsi-23, lsi-11).
corr_cursadas(lsi-23, lsi-18).
corr_cursadas(lsi-24, lsi-11).
corr_cursadas(lsi-24, lsi-18).
corr_cursadas(lsi-24, lsi-19).
corr_cursadas(lsi-25, lsi-20).
corr_cursadas(lsi-25, lsi-16).
corr_cursadas(lsi-26, lsi-11).
corr_cursadas(lsi-26, lsi-15).
corr_cursadas(lsi-27, lsi-11).
corr_cursadas(lsi-27, lsi-16).
corr_cursadas(lsi-27, lsi-19).
corr_cursadas(lsi-28, lsi-22).
corr_cursadas(lsi-29, lsi-24).
corr_cursadas(lsi-30, lsi-15).
corr_cursadas(lsi-30, lsi-23).

% Cuarto Año
corr_cursadas(lsi-31, lsi-20).
corr_cursadas(lsi-31, lsi-29).
corr_cursadas(lsi-32, lsi-29).
corr_cursadas(lsi-33, lsi-29).
corr_cursadas(lsi-34, lsi-24).
corr_cursadas(lsi-35, lsi-22).
corr_cursadas(lsi-36, lsi-21).
corr_cursadas(lsi-37, lsi-33).
corr_cursadas(lsi-37, lsi-34).
corr_cursadas(lsi-38, lsi-30).
corr_cursadas(lsi-39, lsi-32).
corr_cursadas(lsi-39, lsi-33).
corr_cursadas(lsi-40, lsi-30).

% Quinto Año
corr_cursadas(lsi-41, lsi-36).
corr_cursadas(lsi-42, lsi-39).
corr_cursadas(lsi-43, lsi-37).
corr_cursadas(lsi-44, lsi-35).
corr_cursadas(lsi-45, lsi-39).
corr_cursadas(lsi-45, lsi-41).
corr_cursadas(lsi-46, lsi-24).
corr_cursadas(lsi-46, lsi-26).
corr_cursadas(lsi-46, lsi-28).
corr_cursadas(lsi-47, lsi-39).
corr_cursadas(lsi-48, lsi-40).
corr_cursadas(lsi-48, lsi-43).
corr_cursadas(lsi-49, lsi-36).
corr_cursadas(lsi-49, lsi-39).
corr_cursadas(lsi-49, lsi-40).

%! corr_aprobadas(+IdMateria1:number, ?IdMateria2:number) is det.
%
% Para cursar la materia IdMateria1 se necesita la IdMateria2 cursada y aprobada.

% Segundo Año
corr_aprobadas(lsi-11, lsi-2).
corr_aprobadas(lsi-14, lsi-1).
corr_aprobadas(lsi-16, lsi-7).
corr_aprobadas(lsi-17, lsi-3).
corr_aprobadas(lsi-18, lsi-1).
corr_aprobadas(lsi-18, lsi-8).

% Tercer Año
corr_aprobadas(lsi-23, lsi-6).
corr_aprobadas(lsi-24, lsi-10).
corr_aprobadas(lsi-25, lsi-11).
corr_aprobadas(lsi-26, lsi-10).
corr_aprobadas(lsi-27, lsi-12).
corr_aprobadas(lsi-28, lsi-17).
corr_aprobadas(lsi-29, lsi-18).
corr_aprobadas(lsi-30, lsi-18).

% Cuarto Año
corr_aprobadas(lsi-34, lsi-18).
corr_aprobadas(lsi-35, lsi-17).
corr_aprobadas(lsi-36, lsi-27).
corr_aprobadas(lsi-37, lsi-24).
corr_aprobadas(lsi-38, lsi-20).
corr_aprobadas(lsi-38, lsi-23).
corr_aprobadas(lsi-40, lsi-23).

% Quinto Año
corr_aprobadas(lsi-41, lsi-20).
corr_aprobadas(lsi-42, lsi-29).
corr_aprobadas(lsi-43, lsi-25).
corr_aprobadas(lsi-44, lsi-22).
corr_aprobadas(lsi-44, lsi-28).
corr_aprobadas(lsi-45, lsi-32).
corr_aprobadas(lsi-45, lsi-36).
corr_aprobadas(lsi-47, lsi-32).
corr_aprobadas(lsi-47, lsi-33).
corr_aprobadas(lsi-48, lsi-26).
corr_aprobadas(lsi-49, lsi-29).
corr_aprobadas(lsi-49, lsi-28).
