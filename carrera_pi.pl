%% carrera_pi.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% 2024-06-16

:- module(carrera_pi, [
              materia/5,
              corr_cursadas/2,
              corr_aprobadas/2
          ]).
/** <module> carrera_pi: Materias y correlatividades del plan PI 2013

Resumen de materias y cursadas de la carrera Profesorado en Informática (PI),
de la Facultad de Informática de la Universidad Nacional del Comahue (Plan Ord. CS. 1185/2013).

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

%! materia(?Id:number, ?ShortName:atom, ?Name:atom, Year:number, ?Cuatri:number) is det.
%
% Datos de las materias.
% @todo Agregar carga horaria.

% Primer Año
materia(pi-1, pedagogia, 'Pedagogía').
materia(pi-2, rpa, 'Resolución de Problemas y Algoritmos').
materia(pi-3, algebra, 'Elementos de Álgebra').
materia(pi-4, ic, 'Introducción a la Computación').
materia(pi-5, msi, 'Modelos y Sistemas de Información').

materia(pi-6, etc, 'Elementos de Teoría de la Computación').
materia(pi-7, desarrollo, 'Desarrollo de Algoritmos').
materia(pi-8, modelado, 'Modelado de Datos').
materia(pi-9, psicologia1, 'Psicología I').


% Segundo Año
materia(pi-10, calculo, 'Cálculo Diferencial e Integral').
materia(pi-11, poo, 'Programación Orientada a Objetos').
materia(pi-12, tc1, 'Teoría de la Computación I').
materia(pi-13, estructuras, 'Estructura de datos').
materia(pi-14, ingles1, 'Inglés Técnico I').

materia(pi-15, tc2, 'Teoría de la Computación II').
materia(pi-16, requerimientos, 'Ingeniería de Requerimientos').
materia(pi-17, arquitectura1, 'Arquitecturas y Organización de Computadoras I').
materia(pi-18, didactica, 'Didáctica General').
materia(pi-19, ingles2, 'Inglés Técnico II').
materia(pi-20, concurrente, 'Programación Concurrente').

% Tercer Año
materia(pi-21, arquitectura, 'Arquitecturas de Software').
materia(pi-22, so1, 'Sistemas Operativos I').
materia(pi-23, tic, 'Tecnologías de la Información y Comunicación en la Educación').
materia(pi-24, psicologia2, 'Psicología II').

materia(pi-25, metodos, 'Métodos Computacionales para el Cálculo').
materia(pi-26, redes1, 'Redes de Computadoras I').
materia(pi-27, diseno, 'Diseño de Sistemas Informáticos para Educación').
materia(pi-28, labtic, 'Laboratorio de Tecnologías de la Información y Comunicación en la Educación').


% Cuarto Año
materia(pi-29, probabilidad, 'Probabilidad y Estadística').
materia(pi-30, labsi, 'Laboratorio de Sistemas Informáticos para Educación').
materia(pi-31, didactica_esp, 'Didáctica Específica').
materia(pi-32, pea, 'Política Educacional Argentina').

materia(pi-33, aspectos, 'Aspectos Profesionales y Sociales').
materia(pi-34, ciencias, 'Tópicos Avanzados en Ciencias de la Computación').
materia(pi-35, residencia, 'Residencia').


%! corr_cursadas(+IdMateria1:number, ?IdMateria2:number) is det.
%
% Para cursar la materia IdMateria1 se necesita la IdMateria2 cursada.


% Primer Año
corr_cursadas(pi-6, pi-3).
corr_cursadas(pi-7, pi-2).
corr_cursadas(pi-7, pi-3).
corr_cursadas(pi-8, pi-2).
corr_cursadas(pi-8, pi-5).



% Segundo Año
corr_cursadas(pi-10, pi-3).
corr_cursadas(pi-11, pi-7).
corr_cursadas(pi-12, pi-6).
corr_cursadas(pi-12, pi-7).
corr_cursadas(pi-13, pi-6).
corr_cursadas(pi-13, pi-7).
corr_cursadas(pi-14, pi-4).
corr_cursadas(pi-14, pi-5).

corr_cursadas(pi-15, pi-12).
corr_cursadas(pi-16, pi-11).
corr_cursadas(pi-17, pi-12).
corr_cursadas(pi-18, pi-1).
corr_cursadas(pi-19, pi-14).
corr_cursadas(pi-20, pi-4).
corr_cursadas(pi-20, pi-11).

% Tercer Año
corr_cursadas(pi-21, pi-13).
corr_cursadas(pi-21, pi-16).
corr_cursadas(pi-21, pi-19).
corr_cursadas(pi-22, pi-13).
corr_cursadas(pi-22, pi-17).
corr_cursadas(pi-22, pi-20).
corr_cursadas(pi-23, pi-12).
corr_cursadas(pi-23, pi-18).
corr_cursadas(pi-24, pi-9).
corr_cursadas(pi-25, pi-7).
corr_cursadas(pi-25, pi-10).
corr_cursadas(pi-26, pi-22).
corr_cursadas(pi-27, pi-21).
corr_cursadas(pi-27, pi-23).
corr_cursadas(pi-27, pi-24).
corr_cursadas(pi-28, pi-15).
corr_cursadas(pi-28, pi-23).

% Cuarto Año
corr_cursadas(pi-29, pi-25).
corr_cursadas(pi-30, pi-27).
corr_cursadas(pi-30, pi-28).
corr_cursadas(pi-31, pi-27).
corr_cursadas(pi-31, pi-28).
corr_cursadas(pi-32, pi-1).
corr_cursadas(pi-33, pi-26).
corr_cursadas(pi-34, pi-15).
corr_cursadas(pi-34, pi-29).
corr_cursadas(pi-35, pi-30).
corr_cursadas(pi-35, pi-31).
corr_cursadas(pi-35, pi-32).

%! corr_aprobadas(+IdMateria1:number, ?IdMateria2:number) is det.
%
% Para cursar la materia IdMateria1 se necesita la IdMateria2 cursada y aprobada.

% Segundo Año
corr_aprobadas(pi-13, pi-2).
corr_aprobadas(pi-15, pi-6).
corr_aprobadas(pi-16, pi-3).
corr_aprobadas(pi-16, pi-8).
corr_aprobadas(pi-17, pi-4).

% Tercer Año
corr_aprobadas(pi-21, pi-11).
corr_aprobadas(pi-25, pi-3).
corr_aprobadas(pi-26, pi-17).

% Cuarto Año
corr_aprobadas(pi-31, pi-18).
corr_aprobadas(pi-31, pi-23).
corr_aprobadas(pi-33, pi-21).

